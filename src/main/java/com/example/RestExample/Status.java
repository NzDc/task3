package com.example.RestExample;

enum Status {

    IN_PROGRESS,
    COMPLETED,
    CANCELLED;
}
