package com.example.RestExample;

import org.slf4j.ILoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class LoadDatabase {
    Logger log = LoggerFactory.getLogger(LoadDatabase.class);
    @Bean
    CommandLineRunner initDatabase(EmployeeRepository Erepository, OrderRepository Orepository) {
        return args -> {
            log.info("Preloading" + Erepository.save
                    (new Employee("Nazar", "Dochylo","Java back-end developer")));
            log.info("Preloading" + Erepository.save
                    (new Employee("Lena","Golovach","My best friend")));
            log.info("Preloading" + Orepository.save
                    (new Order("MacBook Pro", Status.COMPLETED)));
            log.info("Preloading" + Orepository.save
                    (new Order("Iphone", Status.IN_PROGRESS)));
        };
    }



}
